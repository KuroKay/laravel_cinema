@extends('general.app')

@section('title', 'Page Title')


@section('content')
<div class="container">

    <form method="POST" action="{{ route('artist.update', $artist->id) }}" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        
        <p>
        <label for="lastname">Artist Lastname</label>
        <input type="text" name="lastname" id="lastname" value="{{$artist -> lastname}}" required />
        </p>

        <p>
            <label for="firstname">Artist Firstname</label>
            <input type="text" name="firstname" id="firstname"  value="{{$artist -> firstname}}" required />
            @if ($errors->has('firstname'))
                <div class="invalid-feedback">
                    {{$errors->first('firstname')}}
                </div>
            @endif
        </p>
    
        <p>
            <label for="birthyear">Artist Birthyear</label>
            <input type="text" name="birthyear" id="birthyear"  value="{{$artist -> birthyear}}"  />
            @if ($errors->has('birthyear'))
                <div class="invalid-feedback">
                    {{$errors->first('birthyear')}}
                </div>
            @endif
        </p>
    
        <p>
            <label for="poster">Tronche</label>
            <input type="file" name="poster" id="poster" value="{{$artist -> poster}}" />
            @if ($errors->has('image'))
            <div class="invalid-feedback">
                {{$errors->first('image')}}
            </div>
            @endif
        </p>
        <button type="submit">edit</button>
    </form>
    @if (session('ok'))
        <div class="container">
        <div class="alert alert-dismissible alert-success fade show" role="alert">
        {{ session('ok') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        </div>
    @endif
    </div
@endsection