@extends('general.app')

@section('content')
<div class="container">
    <a class="btn btn-secondary btn-sm active" href="{{ url('/home') }}">Return Login Dashboard</a>
    <table class="table table-striped table-centered">
        <thead>
            <tr>
                <th>{{ __('Lastname') }}</th>
                <th>{{ __('Firstname') }}</th>
                <th>{{ __('Birthyear') }}</th>
                <th>{{ __('Poster') }}</th>
                <th>{{ __('Edit') }}</th>
                <th>{{ __('Delete') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($artists as $artist)
            <tr>
                <td>{{ $artist->lastname }}</td>
                <td>{{ $artist->firstname }}</td>
                <td>{{ $artist->birthyear }}</td>
                <td>{{ $artist->poster }}</td>
                <td>
                    <a href="{{ route('artist.edit', $artist->id) }}" class="btn btn-primary btn-sm"
                        data-toggle="tooltip" title="@lang('Modifier l’artist') {{ $artist->lastname }}">
                        edit
                    </a>
                </td>
                <td>
                    <a href="{{ route('artist.destroy', $artist->id) }}" class="btn btn-danger btn-sm bg-danger"
                        data-toggle="tooltip" title="@lang('Supprimer l’artiste') {{ $artist->lastname }}">
                        delete
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $artists->appends(request()->except('page'))->links() }}
    {{-- @guest('user') --}}
    <a href="{{ route('artist.create') }}" class="btn btn-secondary btn-sm active" data-toggle="tooltip">
        create an new artist
    </a>
    {{-- @endguest    --}}
</div>
@endsection
