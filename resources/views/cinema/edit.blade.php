@extends('general.app')

@section('title', 'Page Title')

@section('content')
<div class="container">
    <form method="POST" action="{{ route('cinema.update', $cinema->id) }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <p>
            <label for="name">Cinema Name</label>
            <input type="text" name="name" id="name" value="{{$cinema -> name}}" required />
        </p>

        <p>
                <label for="address">Cinema Address</label>
                <input type="text" name="address" id="address" value="{{$cinema -> address}}" required />
            </p>

        <button type="submit">edit</button>
    </form>
    @if (session('ok'))
        <div class="container">
        <div class="alert alert-dismissible alert-success fade show" role="alert">
        {{ session('ok') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        </div>
    @endif
</div>
@endsection