@extends('general.app')
@section('content')
<div class="container">
        <a class="btn btn-secondary btn-sm active" href="{{ url('/home') }}">Return Login Dashboard</a>
        <table class="table table-striped table-centered">
            <thead>
                <tr>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Address') }}</th>
                    <th>{{ __('Edit') }}</th>
                    <th>{{ __('Delete') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cinemas as $cinema)
                <tr>
                    <td>{{ $cinema->name }}</td>
                    <td>{{ $cinema->address }}</td>
                    <td>
                        <a href="{{ route('cinema.edit', $cinema->id) }}" class="btn btn-primary btn-sm"
                            data-toggle="tooltip" title="@lang('Modifier cinema') {{ $cinema->title }}">
                            edit
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('cinema.destroy', $cinema->id) }}" class="btn btn-danger btn-sm bg-danger"
                            data-toggle="tooltip" title="@lang('Supprimer cinema') {{ $cinema->title }}">
                            delete
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $cinemas->appends(request()->except('page'))->links() }}
        {{-- @guest('user') --}}
        <a href="{{ route('cinema.create') }}" class="btn btn-secondary btn-sm active" data-toggle="tooltip">
            create an new cinema
        </a>
        {{-- @endguest    --}}
    </div>
@endsection