@extends('general.app')

@section('title', 'Page Title')

@section('content')
<div class="container">
<form method="POST" action="{{ route('cinema.store') }}">
    {{ csrf_field() }}
    <p>
            <label for="name">Cinema name</label>
            <input type="text" name="name" id="name" value="" required />
            @if ($errors->has('name'))
            <div class="invalid-feedback">
                {{$errors->first('name')}}
            </div>
            @endif
        </p>
        <p>
                <label for="address">Cinema address</label>
                <input type="text" name="address" id="address" value="" required />
                @if ($errors->has('address'))
                <div class="invalid-feedback">
                    {{$errors->first('address')}}
                </div>
                @endif
            </p>
    <button type="submit">Créer</button>
</form>
</div>
@endsection