@extends('general.app')

@section('content')
<div class="container">
        <a class="btn btn-secondary btn-sm active" href="{{ url('/home') }}">Return Login Dashboard</a>
        <div class="input-group">
            <input type="text" class="form-control" id="search" name="search">
        </div>
        <table class="table table-striped table-centered">
            <thead>
                <tr>
                    <th>{{ __('Start Time') }}</th>
                    <th>{{ __('End Time') }}</th>
                    <th>{{ __('Room') }}</th>
                    <th>{{ __('Movie') }}</th>
                    <th>{{ __('Edit') }}</th>
                    <th>{{ __('Delete') }}</th>
                </tr>
            </thead>
            <tbody>
                {{-- @foreach($projections as $projection)
                <tr>
                    <td>{{ $projection->startTime }}</td>
                    <td>{{ $projection->endTime }}</td>
                    <td>{{ $projection->room_id }}</td>
                    <td>{{ $projection->movie_id }}</td>
                    <td>
                        <a href="{{ route('projection.edit', $projection->id) }}" class="btn btn-primary btn-sm"
                            data-toggle="tooltip" title="@lang('Modifier projection') {{ $projection->title }}">
                            edit
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('projection.destroy', $projection->id) }}" class="btn btn-danger btn-sm bg-danger"
                            data-toggle="tooltip" title="@lang('Supprimer projection') {{ $projection->title }}">
                            delete
                        </a>
                    </td>
                </tr>
                @endforeach --}}
            </tbody>
        </table>
        {{-- {{ $projections->appends(request()->except('page'))->links() }} --}}
        {{-- @guest('user') --}}
        <a href="{{ route('projection.create') }}" class="btn btn-secondary btn-sm active" data-toggle="tooltip">
            create an new projection
        </a>
        {{-- @endguest    --}}
    </div>
    
@endsection
