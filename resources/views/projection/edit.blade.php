@extends('general.app')

@section('title', 'Page Title')

@section('content')
<div class="container">
    <form method='POST' action="{{ route('projection.update', $projection->id) }}">
        {{method_field('PUT')}}
        {{ csrf_field() }}
        <p>
            <label for="startTime">Start Time</label>
            <input type="datetime" name="startTime" id="startTime" value="{{$projection -> startTime}}" required/>
            @if ($errors->has('startTime'))
            <div class="invalid-feedback">
                {{$errors->first('startTime')}}
            </div>
            @endif
        </p>
        <p>
            <label for="endTime">End Time</label>
            <input type="datetime" name="endTime" id="endTime" value="{{$projection -> endTime}}" required/>
            @if ($errors->has('endTime'))
            <div class="invalid-feedback">
                {{$errors->first('endTime')}}
            </div>
            @endif
        </p>

        <p>
            <label for="room-select">Choose a room:</label>
                <select id="room-select" name="room_id">
                    @foreach ($rooms as $room)
                    <option value="{{$room->id}}">{{ $room->number}}</option>
                    @endforeach
                </select>
            </p>

            <p>
                    <label for="movie-select">Choose a movie:</label>
                        <select id="movie-select" name="movie_id">
                            @foreach ($movies as $movie)
                            <option value="{{$movie->id}}">{{ $movie->title}}</option>
                            @endforeach
                        </select>
                    </p>

        <button type="submit" class="btn btn-danger">Changer</button>
    </form>

    @if (session('ok'))
        <div class="container">
        <div class="alert alert-dismissible alert-success fade show" role="alert">
        {{ session('ok') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        </div>
    @endif
</div>
@endsection