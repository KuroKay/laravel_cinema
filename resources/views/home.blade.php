@extends('general.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>USER Dashboard<h1></div>

                <div class="panel-body">
                  @component('components.who')
                  @endcomponent
                </div>
            </div>
            <div class="see">
                <h2>What do you want to see ?<h2>
                    <a class="btn btn-outline-primary" href="{{ url('/artist') }}">Artist</a>
                    <a class="btn btn-outline-primary" href="{{ url('/movie') }}">Movie</a>
                    <a class="btn btn-outline-primary" href="{{ url('/cinema') }}">Cinema</a>
                    <a class="btn btn-outline-primary" href="{{ url('/room') }}">Room</a>
                    <a class="btn btn-outline-primary" href="{{ url('/projection') }}">Projection</a>
            </div>
        </div>
    </div>
</div>
@endsection
