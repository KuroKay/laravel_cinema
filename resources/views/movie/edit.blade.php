@extends('general.app')

@section('title', 'Page Title')

@section('content')
<div class="container">
    <form method='POST' action="{{ route('movie.update', $movie->id) }}">
        {{method_field('PUT')}}
        {{ csrf_field() }}
        <p>
            <label for="title">title</label>
            <input type="text" name="title" id="title" value="{{$movie -> title}}" required/>
            @if ($errors->has('title'))
            <div class="invalid-feedback">
                {{$errors->first('title')}}
            </div>
            @endif
        </p>
        <p>
            <label for="year">year</label>
            <input type="text" name="year" id="year" value="{{$movie -> year}}" required/>
            @if ($errors->has('year'))
            <div class="invalid-feedback">
                {{$errors->first('year')}}
            </div>
            @endif
        </p>

        <p>
            <label for="artist-select">Choose a artist:</label>
                <select id="artist-select" name="artiste_id">
                    @foreach ($artists as $artist)
                    <option value="{{$artist->id}}">{{ $artist->lastname}}</option>
                    @endforeach
                </select>
            </p>

            <p>
                    <label for="poster">Poster</label>
                    <input type="file" name="poster" id="poster" value="{{$movie -> poster}}" />
                    @if ($errors->has('image'))
                    <div class="invalid-feedback">
                        {{$errors->first('image')}}
                    </div>
                    @endif
                </p>

        <button type="submit" class="btn btn-danger">Changer</button>
    </form>

    @if (session('ok'))
        <div class="container">
        <div class="alert alert-dismissible alert-success fade show" role="alert">
        {{ session('ok') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        </div>
    @endif
</div>
@endsection