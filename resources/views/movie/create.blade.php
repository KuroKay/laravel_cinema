@extends('general.app')

@section('title', 'Page Title')

@section('content')
<div class="container">
<form method='POST' action="{{ route('movie.store') }}">
    {{ csrf_field() }}
    <p>
        <label for="title">title</label>
        <input type="text" name="title" id="title" value="" required />
        @if ($errors->has('title'))
        <div class="invalid-feedback">
            {{$errors->first('title')}}
        </div>
        @endif
    </p>
    <p>
        <label for="year">Year</label>
        <input type="text" name="year" id="year" value="" required />
        @if ($errors->has('year'))
        <div class="invalid-feedback">
            {{$errors->first('year')}}
        </div>
        @endif
    </p>
    <p>
        <label for="artist-select">Choose a artist:</label>
        <select id="artist-select" name="artiste_id">
            @foreach ($artists as $artist)
            <option value="{{$artist->id}}">{{ $artist->lastname}}</option>
            @endforeach
        </select>
    </p>
    <p>
            <label for="poster">Poster</label>
            <input type="file" name="poster" id="poster" />
            @if ($errors->has('image'))
            <div class="invalid-feedback">
                {{$errors->first('image')}}
            </div>
            @endif
        </p>


    <button type="submit" class="btn btn-danger">Créer</button>
</form>
</div>
@endsection
