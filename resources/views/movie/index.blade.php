@extends('general.app')

@section('content')
<div class="container">
        <a class="btn btn-secondary btn-sm active" href="{{ url('/home') }}">Return Login Dashboard</a>
        <table class="table table-striped table-centered">
            <thead>
                <tr>
                    <th>{{ __('Title') }}</th>
                    <th>{{ __('Year') }}</th>
                    <th>{{ __('Artist') }}</th>
                    <th>{{ __('Poster') }}</th>
                    <th>{{ __('Edit') }}</th>
                    <th>{{ __('Delete') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($movies as $movie)
                <tr>
                    <td>{{ $movie->title }}</td>
                    <td>{{ $movie->year }}</td>
                    <td>{{ $movie->artist_id }}</td>
                    <td>{{ $movie->poster }}</td>
                    <td>
                        <a href="{{ route('movie.edit', $movie->id) }}" class="btn btn-primary btn-sm"
                            data-toggle="tooltip" title="@lang('Modifier movie') {{ $movie->title }}">
                            edit
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('movie.destroy', $movie->id) }}" class="btn btn-danger btn-sm bg-danger"
                            data-toggle="tooltip" title="@lang('Supprimer movie') {{ $movie->title }}">
                            delete
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $movies->appends(request()->except('page'))->links() }}
        {{-- @guest('user') --}}
        <a href="{{ route('movie.create') }}" class="btn btn-secondary btn-sm active" data-toggle="tooltip">
            create an new movie
        </a>
        {{-- @endguest    --}}
    </div>
@endsection
