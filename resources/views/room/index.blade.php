@extends('general.app')
@section('content')
<div class="container">
        <a class="btn btn-secondary btn-sm active" href="{{ url('/home') }}">Return Login Dashboard</a>
        <table class="table table-striped table-centered">
            <thead>
                <tr>
                    <th>{{ __('Number') }}</th>
                    <th>{{ __('Conditioner') }}</th>
                    <th>{{ __('Capacity') }}</th>
                    <th>{{ __('Cinema') }}</th>
                    <th>{{ __('Edit') }}</th>
                    <th>{{ __('Delete') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rooms as $room)
                <tr>
                    <td>{{ $room->number }}</td>
                    <td>{{ $room->conditioner }}</td>
                    <td>{{ $room->capacity }}</td>
                    <td>{{ $room->cinema_id }}</td>
                    <td>
                        <a href="{{ route('room.edit', $room->id) }}" class="btn btn-primary btn-sm"
                            data-toggle="tooltip" title="@lang('Modifier room') {{ $room->title }}">
                            edit
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('room.destroy', $room->id) }}" class="btn btn-danger btn-sm bg-danger"
                            data-toggle="tooltip" title="@lang('Supprimer room') {{ $room->title }}">
                            delete
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $rooms->appends(request()->except('page'))->links() }}
        {{-- @guest('user') --}}
        <a href="{{ route('room.create') }}" class="btn btn-secondary btn-sm active" data-toggle="tooltip">
            create an new room
        </a>
        {{-- @endguest    --}}
    </div>
@endsection