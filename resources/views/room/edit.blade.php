@extends('general.app')

@section('title', 'Page Title')

@section('content')
<div class="container">
    <form method='POST' action="{{ route('room.update', $room->id) }}">
        {{method_field('PUT')}}
        {{ csrf_field() }}
        <p>
            <label for="number">Room Number</label>
            <input type="numbers" name="number" id="number" value="{{$room -> number}}" required/>
            @if ($errors->has('number'))
            <div class="invalid-feedback">
                {{$errors->first('number')}}
            </div>
            @endif
        </p>
        <p>
            <label for="conditioner">Room Conditioner</label>
            <input type="text" name="conditioner" id="conditioner" value="{{$room -> conditioner}}" required/>
            @if ($errors->has('conditioner'))
            <div class="invalid-feedback">
                {{$errors->first('conditioner')}}
            </div>
            @endif
        </p>

        <p>
                <label for="capacity">Room Capacity</label>
                <input type="number" name="capacity" id="capacity" value="{{$room -> capacity}}" required/>
                @if ($errors->has('capacity'))
                <div class="invalid-feedback">
                    {{$errors->first('capacity')}}
                </div>
                @endif
            </p>

            <p>
            <label for="cinema-select">Choose a cinema:</label>
                <select id="cinema-select" name="cinema_id">
                    @foreach ($cinemas as $cinema)
                    <option value="{{$cinema->id}}">{{ $cinema->name}}</option>
                    @endforeach
                </select>
            </p>

        <button type="submit" class="btn btn-danger">Changer</button>
    </form>

    @if (session('ok'))
        <div class="container">
        <div class="alert alert-dismissible alert-success fade show" role="alert">
        {{ session('ok') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        </div>
    @endif
</div>
@endsection