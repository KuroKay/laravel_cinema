@extends('general.app')

@section('title', 'Page Title')

@section('content')
<div class="container">
<form method='POST' action="{{ route('room.store') }}">
    {{ csrf_field() }}
    <p>
        <label for="number">Room Number</label>
        <input type="number" name="number" id="number" value="" required />
        @if ($errors->has('number'))
        <div class="invalid-feedback">
            {{$errors->first('number')}}
        </div>
        @endif
    </p>
    <p>
        <label for="conditioner">Room Conditioner</label>
        <input type="text" name="conditioner" id="conditioner" value="" required />
        @if ($errors->has('conditioner'))
        <div class="invalid-feedback">
            {{$errors->first('conditioner')}}
        </div>
        @endif
    </p>
    <p>
            <label for="capacity">Room Capacity</label>
            <input type="capacity" name="capacity" id="capacity" value="" required />
            @if ($errors->has('capacity'))
            <div class="invalid-feedback">
                {{$errors->first('capacity')}}
            </div>
            @endif
        </p>
    <p>
        <label for="cinema-select">Choose a cinema:</label>
        <select id="cinema-select" name="cinema_id">
            @foreach ($cinemas as $cinema)
            <option value="{{$cinema->id}}">{{ $cinema->name}}</option>
            @endforeach
        </select>
    </p>


    <button type="submit" class="btn btn-danger">Créer</button>
</form>
</div>
@endsection