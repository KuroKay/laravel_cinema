<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MovieRequest;
use App\Models\Movie;
use App\Models\Artist;
use App\Notifications\MovieCreated;


class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('movie.index', [ 'movies' => Movie::paginate() ]);
    }

    public static function getEloquentSqlWithBindings($query)
	{
	    return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
	        return is_numeric($binding) ? $binding : "'{$binding}'";
	    })->toArray());
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movie.create', ['artists' => Artist::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovieRequest $request)
    {
        $movie = Movie::create($request->all());

        Auth()->user()->notify(new MovieCreated($movie));

        $poster = $request->file('poster');
        $filename = 'poster_'.$movie->id . '.'. $poster->getClientOriginalExtension();
        $image = Image::make($poster)->fit(180,240)
                                        ->save(public_path('/uploads/posters/'.$filename));

        return redirect ()->route ('movie.index')
                          ->with ('ok', __ ('le film a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        $this->authorize('movie.edit', $movie);
        return view('movie.edit', ['movie' => $movie]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MovieRequest $request, Movie $movie)
    {
        $this->authorize('movie.update', $movie);
        $movie->update( $request->all() );

        //Affiche film
        $poster = $request->file('poster');
        $filename = 'poster_'.$movie->id . '.'. $poster->getClientOriginalExtension();
        InterventionImage::make($poster)->fit(180,240)
                                        ->save(public_path('/uploads/posters/'.$filename));

        return redirect()->route('movie.index')
                        ->with( 'ok', __('movie a bien été modifié') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $this->authorize('movie.destroy', $movie);
        $movie->delete();
        return response()->json();
    }
    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
        $this->middleware('auth')->only('create');
    }
}
