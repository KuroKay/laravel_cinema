<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Artist;
use App\Http\Requests\ArtistRequest;
use App\Notifications\ArtistCreated;
use Illuminate\Support\Facades\Storage;
use Image;


class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artist.index', [ 'artists' => Artist::paginate() ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArtistRequest $request)
    {
        $artist = Artist::create($request->all());

        //Notification
        Auth()->user()->notify(new ArtistCreated($artist));

        //Image
        $poster = $request->file( 'poster' );
        $filename = 'poster_' . $artist->id . '.' . $poster->getClientOriginalExtension();
        Image::make( $poster )->fit( 180, 240 )
        ->save( public_path( '/uploads/posters/' . $filename ) );

        //Rediraction
        return redirect()->route ('artist.index')
                         ->with ('ok',__('artiste a bien été enregistré') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist)
    {
        //Autorisation editer
        // after
        $this->authorize('artist.edit', $artist);

        return view('artist.edit', ['artist' => $artist]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArtistRequest $request, Artist $artist)
    {
        //Autorisation update

        $this->authorize('artist.update', $artist);
        $artist->update( $request->all() );

        // image
        $poster = $request->file('poster');
        $filename = 'poster_'.$artist->id . '.'. $poster->getClientOriginalExtension();
        InterventionImage::make($poster)->fit(180,240)
                                        ->save(public_path('/uploads/posters/'.$filename));
        return redirect()->route('artist.index')
                         ->with( 'ok', __('artiste a bien été modifié'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        //Autorisation delete

        $this->authorize('artist.destroy', $artist);
        
        $artist->delete();
        return response()->json();
    }

    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
        $this->middleware('auth')->only('create');
    }
}
