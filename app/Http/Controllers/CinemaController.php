<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CinemaRequest;
use App\Models\Cinema;

class CinemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cinema.index', [ 'cinemas' => Cinema::paginate() ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cinema.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CinemaRequest $request)
    {
        Cinema::create($request->all());
        return redirect ()->route ('cinema.index')
                            ->with ('ok', __ ('cinema a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cinema $cinema)
    {
        $this->authorize('cinema.edit', $cinema);

        return view('cinema.edit', ['cinema' => $cinema]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CinemaRequest $request, Cinema $cinema)
    {
        $this->authorize('cinema.update', $cinema);

        $cinema->update( $request->all() );
        return redirect()->route('cinema.index')
                        ->with( 'ok', __('cinema a bien été modifié') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cinema $cinema)
    {
        $this->authorize('cinema.delete', $cinema);

        $cinema->delete();
        return response()->json();
    }
    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
    }
}
