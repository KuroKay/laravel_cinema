<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectionRequest;
use App\Models\Projection;

class ProjectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('projection.index', [ 'projections' => Projection::paginate() ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projection.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectionRequest $request)
    {
        $projection = Projection::create($request->all());
        return redirect ()->route ('projection.index')
                          ->with ('ok', __ ('la scéance a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Projection $projection)
    {
        $this->authorize('projection.edit', $projection);
        return view('projection.edit', ['projection' => $projection]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectionRequest $request, Projection $projection)
    {
        $projection->update( $request->all() );
        $this->authorize('projection.update', $projection);
        return redirect()->route('projection.index')
                        ->with( 'ok', __('projection a bien été modifié') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Projection $projection)
    {
        $this->authorize('projection.destroy', $projection);
        $projection->delete();
        return response()->json();
    }
    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
    }

    public function search(ProjectionRequest $request){

        if($request->ajax()){
            $output="";
            $projections=DB::table('projections')->where('startTime', 'LIKE', '%'.$request->search.'%')
                                                ->orWhere('endtime', 'LIKE', '%'.$request->search.'%')->get;
        }



        if($projections){
            foreach($projections as $key => $projection){
                $output =
                    '<tr>'.
                    '<td>'.$projection->startTime.'</td>'.
                    '<td>'.$projection->endTime.'</td>'.
                    '<td>'.$projection->room_id.'</td>'.
                    '<td>'.$projection->movie_id.'</td>'.
                '</tr>';

            }
            return Response($output);
        }
    }
}
