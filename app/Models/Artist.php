<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $fillable = [
        'lastname', 'firstname', 'birthyear'
    ];

    public function starredMovies()
    {
        return $this->hasMany('App\Models\Movie');
    }

    public function playedMovies()
    {
    return $this->belongsToMany('App\Models\Movie')->withPivot('role_name');
    }
}
