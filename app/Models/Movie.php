<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = [
        'title', 'year'
    ];

    public function director()
    {
        return $this->belongsTo('App\Models\Artist');
    }

    public function actors()
    {
        return $this->belongsToMany('App\Models\Artist');
    }

    public function movie_projection()
    {
        return $this->belongsToOne('App\Models\Projection');
    }
}
