<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Projection extends Model
{

    protected $fillable = [
        'startTime', 'endTime'
    ];

    public function room_projection()
    {
        return $this->hasOne('App\Models\Room');
    }

    public function movie_projection()
    {
        return $this->hasOne('App\Models\Movie');
    }
}
