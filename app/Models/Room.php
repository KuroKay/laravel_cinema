<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'number', 'conditioner','capacity'
    ];

    public function director()
    {
        return $this->belongsToOne('App\Models\Cinema');
    }

    public function projection_room()
    {
        return $this->belongsToOne('App\Models\Projection');
    }
}
