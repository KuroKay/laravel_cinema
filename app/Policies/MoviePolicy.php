<?php

namespace App\Policies;

use App\User;
use App\Models\ { Movie, Cinema, Projection, Room, Artist };
use Illuminate\Auth\Access\HandlesAuthorization;

class MoviePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function manage(User $user, Movie $movie)
    {
        return $user->id === $movie->user_id;
    }
}
