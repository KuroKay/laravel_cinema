<?php

use Illuminate\Database\Seeder;
use App\Models\Movie;
use App\Models\Projection;
use App\Models\Room;

class ProjectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projection = new Projection ([
            'startTime' => '2019-04-04 04:03:20',
            'endTime' => '2019-04-04 06:03:20',
            'movie_id' => Movie::where('title', 'Split')->first()->id,
            'room_id' => Room::where('number', '1')->first()->id
        ]);
        $projection -> save();
    }
}
