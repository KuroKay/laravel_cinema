<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'name' => 'kare',
            'email' => 'karen.tschanz@crea-inseec.com',
            'password' => 'karen',
            ],[
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => 'admin',
            ]
        ]);
    }
}
