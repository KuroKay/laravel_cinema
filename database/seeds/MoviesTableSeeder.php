<?php

use Illuminate\Database\Seeder;

use App\Models\Artist;
use App\Models\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([
                [
                'title' => 'Kong: Skull Island',
                'year' => '2017',
                ],
                [
                'title' => 'Mulholland Drive',
                'year' => '2001',
                ],
                [
                'title' => 'Café Society',
                'year' => '2016',
                ],
            ]);

            $movie = new Movie ([
                'title' => 'Split',
                'year' => '2017',
                'artist_id' => Artist::where('lastname', 'McAvoy')->first()->id
            ]);
            $movie -> save();
    }
}
