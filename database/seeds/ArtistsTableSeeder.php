<?php

use Illuminate\Database\Seeder;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artists')->insert([
                [
                'lastname' => 'Allen',
                'firstname' => 'Woody',
                'birthyear' => 1938,
                ],
                [
                'lastname' => 'Lynch',
                'firstname' => 'David',
                'birthyear' => 1946
                ],
                [
                'lastname' => 'Hiddleston',
                'firstname' => 'Tom',
                'birthyear' => 1983
                ],
                [
                'lastname' => 'McAvoy',
                'firstname' => 'James',
                'birthyear' => 1985
                ]
            ]);
    }
}
