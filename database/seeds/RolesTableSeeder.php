<?php

use Illuminate\Database\Seeder;
use App\Models\Artist;
use App\Models\Movie;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artist_movie')->insert([
            [
            'role_name' => 'la bête',
            ]
        ]);
        
        $role = new Role ([
            'role_name' => 'explorateur',
            'movie_id' => Movie::where('title', 'Kong: Skull Island')->first()->id,
            'artist_id' => Artist::where('lastname', 'Hiddleston')->first()->id
        ]);
        $role -> save();
    }
}
