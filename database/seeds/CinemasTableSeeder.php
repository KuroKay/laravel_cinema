<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cinemas')->insert([
                [
                'name' => 'Cinelux',
                'address' => 'Boulevard de Saint-Georges 8, 1205 Genève',
                ],
                [
                'name' => 'Pathé Balexert',
                'address' => 'Avenue Louis-Casaï 27, 1211 Genève',
                ],
                [
                'name' => 'Arena Cinemas La Praille',
                'address' => 'Route des Jeunes 10, 1227 Carouge',
                ],
                [
                'name' => 'Cinéma Bio',
                'address' => 'Rue Saint-Joseph 47, 1227 Carouge',
                ]
            ]);
    }
}
