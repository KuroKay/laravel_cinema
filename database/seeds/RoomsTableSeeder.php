<?php

use Illuminate\Database\Seeder;
use App\Models\Cinema;
use App\Models\Room;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
                [
                'number' => 1,
                'conditioner' => 'yes',
                'capacity' => 120,
                ],
                [
                'number' => 2,
                'conditioner' => 'non',
                'capacity' => 200,
                ],
                [
                'number' => 3,
                'conditioner' => 'yes',
                'capacity' => 240,
                ]
            ]);

            $room = new Room ([
                'number' => 4,
                'conditioner' => 'non',
                'capacity' => 50,
                'cinema_id' => Cinema::where('name', 'Cinelux')->first()->id,
            ]);
            $room -> save();
    }
}
