<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArtistsTableSeeder::class);
        $this->call(MoviesTableSeeder::class);
        $this->call(CinemasTableSeeder::class);
        $this->call(RoomsTableSeeder::class);
        $this->call(ProjectionsTableSeeder::class);
        // $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
    }
}
