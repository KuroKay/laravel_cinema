<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            [
            'name' => 'teacher',
            'email' => 'teacher@teacher.com',
            'password' => 'teacher00',
            'job_title' => 'teacher',
            ]
        ]);
    }
}
